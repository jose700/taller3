/* Funcion para calular el area de un triangulo*/
function area(calcular) {

    /*Definimos nuestra variables y obtenemos la id de nuestro input*/
    var num1 = document.getElementById("n1").value;
    var num2 = document.getElementById("n2").value;
    //Calculamos el area en este caso num1 * num2 / 2
    calcular = num1 * num2 / 2;
    //Imprimimos resultado
    alert("el resultado es: " + calcular);
}
/*Funcion para calcular la raiz de un numero*/
function raiz() {

    var n1 = document.getElementById("nimpar").value;
    var resultado = Math.sqrt(n1);
    alert("el resultado de la raiz cuadrada es: " + resultado.toFixed(3));

}
//Funcion para mostrar la hora actual en segundos
function actual() {
    var hora = new Date();
    document.getElementById("segundos").value = (hora.getHours() * 3600) +
        (hora.getMinutes() * 60) + hora.getSeconds();
    Alert("la hora actual en segundos es" + hora);
}

/*Array para mostrar los dias de la semana*/
function dias() {
    var semana = ["Lunes \n",
        "Martes \n", "Miercoles \n", "Jueves \n", "Viernes"
    ];
    var fines = ["Sabado \n", "Domingo \n"];
    alert(semana + fines);

}
//funcion para obtener el numero de caracteres y el numero de palabras
function wordCount() {
    var textoArea = document.getElementById("caracteres").value;
    var numeroCaracteres = textoArea.length;
    var inicioBlanco;
    var finBlanco;
    var variosBlancos = /[ ]+/g;
    textoArea = textoArea.replace(inicioBlanco, "");
    textoArea = textoArea.replace(finBlanco, "");
    textoArea = textoArea.replace(variosBlancos, " ");
    var textoAreaDividido = textoArea.split(" ");
    var numeroPalabras = textoAreaDividido.length;
    var tC = (numeroCaracteres == 1) ? " carácter" : " caracteres";
    var tP = (numeroPalabras == 1) ? " palabra" : " palabras";
    alert(numeroCaracteres + tC + "\n" + numeroPalabras + tP);
}
/*Version del navegador*/
function version() {
    var txt = document.getElementById("mostrar").value;
    txt = navigator.appCodeName;
    txt = navigator.language;
    txt = navigator.onLine;
    txt = navigator.platform;
    txt = navigator.userAgent;
    //Imprimimos
    alert(txt);
}
// Función para obtener el Ancho(Width) 
function obtenerAncho(obj, ancho) {
    $("#ancho").text("El ancho de la " + obj + " es " + ancho + "px. (Width)");
}

// Función para obtener el Alto(Height)  
function obtenerAlto(obj, alto) {
    $("#alto").text("El alto de la " + obj + " es " + alto + "px. (Height)");
}
obtenerAlto("ventana", $(window).height());
obtenerAncho("ventana", $(window).width());
$(window).resize(function() {


    obtenerAlto("ventana", $(window).height());
    obtenerAncho("ventana", $(window).width());


});